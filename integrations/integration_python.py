

import requests

host = "http://0.0.0.0:9002"
user_email = "your_email@gmail.com"
user_password = "dummy_password"

endpoints = {
    "get_token": host + "/api-token-auth/",
    "profiles": host + "/profiles/",
    "apps": host + "/apps/",
    "logs": host + "/logs/",
}


# Register
register_data = {
    "email": user_email,
    "password": user_password,
}

response = requests.post(endpoints["profiles"], data=register_data)
registration_data = response.json()
# {
#     'id': 4, 
#     'url': 'http://0.0.0.0:9002/profiles/4/', 
#     'username': 'your_email@gmail.com', 
#     'email': 'your_email@gmail.com', 
#     'account_state': 2
# }



# Get token
def get_jwt_token():
    auth_data = {
        "username": user_email,
        "password": user_password,
    }
    response = requests.post(endpoints["get_token"], data=auth_data)
    jwt_token_response = response.json()
    # {
    #     'token': 'YOUR_TOKEN_A.YOUR_TOKEN_B.YOUR_TOKEN_C'
    # }
    return jwt_token_response["token"]

def get_auth_header():
    return {
        "Authorization": "JWT {token}".format(token=get_jwt_token())
    }

# Create App
create_app_data = {
    "name": "My App Name",
    "description": "My App Description",
}

response = requests.post(endpoints["apps"], data=create_app_data, headers=get_auth_header())
app_data = response.json()
# {
#     'id': 6, 
#     'name': 'My App Name', 
#     'public_key': 'your_public_key', 
#     'private_key': 'your_private_key'
# }
application_pk = app_data["id"]


# Create LogEntry
create_log_data = {
    "application": application_pk,
    "level": 10,
    "short_content": "Log Entry Short Content",
    "content": "Log Entry Details",
    "time": None
}

response = requests.post(endpoints["logs"], data=create_log_data, headers=get_auth_header())
log_data = response.json()
# {
#     'id': 3, 
#     'url': 'http://0.0.0.0:9002/logs/3/', 
#     'application': 6, 
#     'application__name': 'My App Name', 
#     'level': 10, 
#     'level__name': 'Error', 
#     'short_content': 'Log Entry Short Content', 
#     'content': 'Log Entry Details', 
#     'time': '2018-06-03T18:43:21.770113Z'
# }

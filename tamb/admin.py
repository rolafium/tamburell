from django.contrib import admin
from tamb.models import Application, LogEntry, UserProfile

admin.site.register(Application)
admin.site.register(LogEntry)
admin.site.register(UserProfile)

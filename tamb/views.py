from django.contrib.auth.models import User
from django.http import Http404
from django.shortcuts import render
from rest_framework import generics, status, viewsets, permissions
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.views import APIView
from tamb.models import Application, AppCredentials, LogEntry, UserProfile, Token
from tamb.permissions import IsOwner, IsOwnerOrCreate, IsApplicationOwner
from tamb.serializers import (
    ProfileCreationSerializer,
    ProfileUpdateSerializer,
    ProfileSerializer, 
    ApplicationSerializer, 
    LogEntrySerializer
)


class ProfileViewSet(viewsets.ModelViewSet):
    permission_classes = (IsOwnerOrCreate,)

    def get_queryset(self):
        user = self.request.user
        if user.is_superuser:
            return UserProfile.objects.all()
        else:
            return UserProfile.objects.filter(user_id=user.id)

    def get_serializer_class(self):
        if self.action == "create":
            return ProfileCreationSerializer
        elif self.action == "update":
            return ProfileUpdateSerializer
        else:
            return ProfileSerializer

    @action(detail=True)
    def confirm_email(self, request, *args, **kwargs):
        profile = self.get_object()
        if profile.account_state != UserProfile.AccountStates.EMAIL_UNCONFIRMED.code:
            return Response({}, status=status.HTTP_400_BAD_REQUEST)
        try:
            token = Token.objects.get(
                user=request.user,
                token_type=Token.EMAIL_CONFIRMATION,
                token_value=request.GET.get("email_confirmation_token"),
            )
            token.consumed = True
            token.save()
            profile.account_state = UserProfile.AccountStates.READY.code
            profile.save()
            return Response({ "email_confirmed" : True }, status=status.HTTP_200_OK)
        except Token.DoesNotExist:
            return Response({ "email_confirmed" : False }, status=status.HTTP_404_NOT_FOUND)
        except:
            return Response({ "email_confirmed" : False }, status=status.HTTP_500_INTERNAL_ERROR)

    def update(self, request, pk, *args, **kwargs):
        serializer = ProfileUpdateSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save(pk)
            context = {
                "request": request,
            }
            profile = ProfileSerializer(user.profile, context=context)
            return Response(profile.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def create(self, request, *args, **kwargs):
        serializer = ProfileCreationSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            context = {
                "request": request,
            }
            profile = ProfileSerializer(user.profile, context=context)
            return Response(profile.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ApplicationViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated, IsOwner)
    serializer_class = ApplicationSerializer

    def get_queryset(self):
        user = self.request.user
        if user.is_superuser:
            return Application.objects.all()
        else:
            return Application.objects.filter(user_id=user.id)


    def create(self, request):
        if not request.user.is_superuser:
            existing_apps_num = Application.objects.filter(user=request.user).count()
            if existing_apps_num > Application.USER_APPLICATION_LIMIT:
                return Response(status=status.HTTP_412_PRECONDITION_FAILED)

        serializer = ApplicationSerializer(data=request.data)
        if serializer.is_valid():
            app = AppCredentials.create_app(request)
            return Response(app.credentials.to_dict(), status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LogEntryViewSet(viewsets.ModelViewSet):
    queryset = LogEntry.objects.all()
    serializer_class = LogEntrySerializer
    permission_classes = (permissions.IsAuthenticated, IsApplicationOwner)

    def get_queryset(self):
        user = self.request.user
        if user.is_superuser:
            return LogEntry.objects.all()
        else:
            return LogEntry.objects.filter(application__user_id=user.id)


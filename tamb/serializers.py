from rest_framework import serializers
from tamb.models import Application, UserProfile, LogEntry


class ProfileCreationSerializer(serializers.Serializer):
    email = serializers.CharField()
    password = serializers.CharField(style={ "input_type": "password" })

    class Meta:
        fields = ("email", "password",)

    def save(self):
        return UserProfile.create_profile(self.data)


class ProfileUpdateSerializer(serializers.Serializer):
    password = serializers.CharField(write_only=True, style={ "input_type": "password" })

    class Meta:
        fields = ('password',)

    def save(self, user_id):
        return UserProfile.update_profile(user_id, self.validated_data)


class ProfileSerializer(serializers.ModelSerializer):
    username = serializers.CharField(read_only=True, source="user.username")
    email = serializers.CharField(read_only=True, source="user.email")
    account_state = serializers.IntegerField(read_only=True)

    class Meta:
        model = UserProfile
        fields = (
            "id",
            "url",
            "username",
            "email",
            "account_state",
        )


class ApplicationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Application
        fields = (
            "id", 
            "url",
            "name",
            "user",
            "description", 
            "public_key", 
            "creation_date",
        )

    user = serializers.ReadOnlyField(source='user.email')
    public_key = serializers.CharField(read_only=True)


class LogEntrySerializer(serializers.ModelSerializer):

    class ApplicationForeignKey(serializers.PrimaryKeyRelatedField):
        def get_queryset(self):
            return Application.objects.filter(user=self.context["request"].user)

    application = ApplicationForeignKey()
    application__name = serializers.CharField(read_only=True, source="application.name")
    level__name = serializers.CharField(read_only=True, source="get_level_display")

    class Meta:
        model = LogEntry
        fields = (
            "id",
            "url",
            "application",
            "application__name",
            "level",
            "level__name",
            "short_content",
            "content",
            "time",
        )

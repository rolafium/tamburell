
import string
import smtplib
from hashlib import sha256
from random import choice
from django.conf import settings


ALPHABET = string.digits + string.ascii_letters

def generate_random_str(l=64):
    letters = []

    for i in range(l):
        letters.append(choice(ALPHABET))

    return "".join(letters)


def generate_hash(s):
    hs = sha256(s.encode("utf-8")).hexdigest()
    return hs


def send_email(receivers, message):
    sender = settings.EMAIL_SMTP_SENDER
    server = smtplib.SMTP(settings.EMAIL_SMTP_HOST, settings.EMAIL_SMTP_PORT)
    server.login(settings.EMAIL_SMTP_USERNAME, settings.EMAIL_SMTP_PASSWORD)
    server.sendmail(sender, receivers, message)

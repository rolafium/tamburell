from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.urls import reverse
from django.utils import timezone
from tamb.utils import generate_random_str, generate_hash, send_email


class ModelChoice:
    def __init__(self, code, name):
        self.code = code
        self.name = name

    def to_tuple(self):
        return (self.code, self.name,)

    def __str__(self):
        return self.name


class ModelChoiceList:

    @classmethod
    def get_choices(cls):
        choices = []

        for attr_name in cls.__dict__:
            attr = cls.__dict__[attr_name]
            if isinstance(attr, ModelChoice):
                choices.append(attr)

        return choices

    @classmethod
    def get_choices_tuple(cls):
        choices_tuples = []

        for attr_name in cls.__dict__:
            attr = cls.__dict__[attr_name]
            if isinstance(attr, ModelChoice):
                choice_tuple = (attr.code, attr.name)
                choices_tuples.append(choice_tuple)

        return choices_tuples


class AppCredentials:

    def __init__(self):
        self.public_key = generate_random_str(64)
        self.private_key = generate_random_str(64)
        self.private_key_hash = generate_hash(self.private_key)

    def set_app(self, app):
        self.app_id = app.id
        self.name = app.name
        self.description = app.description

        app.public_key = self.public_key
        app.private_key = self.private_key_hash

        app.save()

        return app

    def to_dict(self):
        return {
            "id": self.app_id,
            "name": self.name,
            "public_key": self.public_key,
            "private_key": self.private_key,
        }

    @classmethod
    def create_app(cls, request):
        app = Application()
        app.name = request.data["name"]
        app.user = request.user
        app.description = request.data["description"]

        credentials = cls()
        credentials.set_app(app)

        credentials.app_id = app.id
        app.credentials = credentials

        return app

    def __str__(self):
        return "Credential generator for App #%s" % self.app.name


class Application(models.Model):

    USER_APPLICATION_LIMIT = 5

    name = models.CharField(max_length=128)
    user = models.ForeignKey("auth.User", on_delete=models.CASCADE)
    description = models.TextField()
    public_key = models.CharField(max_length=64)
    private_key = models.TextField(max_length=64)
    creation_date = models.DateTimeField(auto_now_add=True)

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "user_id": self.user_id,
            "description": self.description,
            "public_key": self.public_key,
            "private_key": self.private_key,
            "creation_date": str(self.creation_date)
        }

    def __str__(self):
        return self.name



class LogEntry(models.Model):

    class Levels(ModelChoiceList):
        ERROR = ModelChoice(10, "Error")
        WARN  = ModelChoice(20, "Warn")
        INFO  = ModelChoice(30, "Info")
        DEBUG = ModelChoice(40, "Debug")
        TRACE = ModelChoice(50, "Trace")

    LEVELS_CHOICES = Levels.get_choices_tuple()

    application = models.ForeignKey("tamb.Application", on_delete=models.CASCADE)
    level = models.IntegerField(choices=LEVELS_CHOICES)
    short_content = models.TextField(default="")
    content = models.TextField(default="")
    time = models.DateTimeField(default=timezone.now)

    def to_dict(self):
        return {
            "id": self.id,
            "application_id": self.application_id,
            "level": self.level,
            "level_name": self.get_level_display(),
            "short_content": self.short_content,
            "content": self.content,
            "time": str(self.time),
        }

    def __str__(self):
        return "%s - %s - %s" % (self.application, self.level, self.time)



class UserProfile(models.Model):

    class AccountStates(ModelChoiceList):
        UNREGISTRED = ModelChoice(0, "Unregistred")
        REGISTRED = ModelChoice(1, "Registred")
        EMAIL_UNCONFIRMED = ModelChoice(2, "Email Unconfirmed")
        READY = ModelChoice(3, "Ready")
        SUSPENDEND = ModelChoice(4, "Suspended")

    ACCOUNT_STATES = AccountStates.get_choices_tuple()

    user = models.ForeignKey("auth.User", on_delete=models.CASCADE)
    account_state = models.IntegerField(default=AccountStates.UNREGISTRED.code, choices=ACCOUNT_STATES)

    @classmethod
    def create_profile(cls, data):
        user = User.objects.create_user(
            data["email"], 
            email=data["email"], 
            password=data["password"],
        )
        user.profile = cls.objects.create(
            user=user,
            account_state=cls.AccountStates.EMAIL_UNCONFIRMED.code,
        )

        user.token = Token.objects.create(
            user=user,
            token_type=Token.EMAIL_CONFIRMATION,
            token_value=generate_random_str(64)
        )

        confirm_location = settings.ALLOWED_HOSTS[0] + reverse("userprofile-confirm-email", args={ user.profile.id })
        confirm_location += "?email_confirmation_token=" + user.token.token_value
        email_message = "Please activate your account by visiting the following address: " + confirm_location
        
        send_email([ data["email"] ], email_message)

        return user

    @classmethod
    def update_profile(cls, user_id, data):
        user = User.objects.get(userprofile__id=user_id)
        user.set_password(data["password"])
        user.save()

        user.profile = cls.objects.get(pk=user_id)
        return user


    def __str__(self):
        return "Profile %s" % self.user



class Token(models.Model):

    EMAIL_CONFIRMATION = "EMAIL_CONFIRMATION"

    user = models.ForeignKey("auth.User", on_delete=models.CASCADE)
    token_type = models.CharField(max_length=64)
    token_value = models.TextField()
    consumed = models.BooleanField(default=False)

    def __str__(self):
        return "User: {user} - Token: {token_type}".format(
            user=self.user,
            token_type=self.token_type,
        )

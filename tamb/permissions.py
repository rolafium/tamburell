from rest_framework import permissions


class IsOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return request.user.is_superuser or obj.user_id == request.user.id


class IsOwnerOrCreate(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method == "POST":
            return True

        return request.user.is_superuser or obj.user_id == request.user.id


class IsApplicationOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return request.user.is_superuser or obj.application.user_id == request.user.id

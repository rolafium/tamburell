from django.conf.urls import include, url
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token
from tamb.views import ProfileViewSet, ApplicationViewSet, LogEntryViewSet




router = routers.DefaultRouter()
router.register(r"profiles", ProfileViewSet, base_name="userprofile")
router.register(r"apps", ApplicationViewSet, base_name="application")
router.register(r"logs", LogEntryViewSet, base_name="logentry")


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^api-token-auth/', obtain_jwt_token),
]

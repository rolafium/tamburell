
from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(settings.DJANGO_ADMIN_SITE_URL, admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls')),
    url(r"^", include("tamb.urls")),
]

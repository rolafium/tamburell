
import os
from tamburell.version import VERSION

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'tamb',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'tamburell.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'tamburell.wsgi.application'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication',
    ),
}

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True
REQUEST_EXPIRE_MINUTES = 24 * 60

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
DEBUG = False
ALLOWED_HOSTS = []
SECRET_KEY = ""
STATIC_URL = ""
DJANGO_ADMIN_SITE_URL = ""
DATABASES = {}
BUILD_NUMBER = "x"
MAIN_LOGGER = "main_logger"
EMAIL_SMTP_USERNAME = ""
EMAIL_SMTP_PASSWORD = ""
EMAIL_SMTP_HOST = ""
EMAIL_SMTP_PORT = ""
EMAIL_SMTP_SENDER = ""


SERVER_TYPE = os.environ.get("TAMBURELL_SERVER_TYPE", "DEVELOPMENT")

if SERVER_TYPE == "HEROKU":
    # Heroku Settings
    import re
    import django_heroku
    django_heroku.settings(locals())

    DEBUG = os.environ.get("TAMBURELL_DEBUG", "0") == "1"
    ALLOWED_HOSTS = os.environ.get("TAMBURELL_ALLOWED_HOSTS", "").split(",")
    SECRET_KEY = os.environ.get("TAMBURELL_DJANGO_SECRET_KEY")
    STATIC_URL = os.environ.get("TAMBURELL_DJANGO_STATIC_URL")
    DJANGO_ADMIN_SITE_URL = os.environ.get("TAMBURELL_DJANGO_ADMIN_SITE_URL")

    # Emails
    EMAIL_SMTP_USERNAME = os.environ.get("TAMBURELL_EMAIL_SMTP_USERNAME", "")
    EMAIL_SMTP_PASSWORD = os.environ.get("TAMBURELL_EMAIL_SMTP_PASSWORD", "")
    EMAIL_SMTP_HOST = os.environ.get("TAMBURELL_EMAIL_SMTP_HOST", "")
    EMAIL_SMTP_PORT = int(os.environ.get("TAMBURELL_EMAIL_SMTP_PORT", ""))

    _build_number_matches = re.findall(r"\d+", os.environ.get("HEROKU_RELEASE_VERSION", "0"))
    if len(_build_number_matches) == 1:
        BUILD_NUMBER = _build_number_matches[0]

elif SERVER_TYPE == "TRAVIS":
    SECRET_KEY = "TRAVIS"
    STATIC_URL = "/static/"
    DJANGO_ADMIN_SITE_URL = "django_admin_site/"
    DATABASES = {
        'default': {
            'ENGINE':   'django.db.backends.postgresql_psycopg2',
            'NAME':     'travisci',
            'USER':     'postgres',
            'PASSWORD': '',
            'HOST':     'localhost',
            'PORT':     '',
        },
        'default': {
            'ENGINE':   'django.db.backends.postgresql_psycopg2',
            'NAME':     'travisci_test',
            'USER':     'postgres',
            'PASSWORD': '',
            'HOST':     'localhost',
            'PORT':     '',
        }
    }

else:
    # Local Settings
    import tamburell.local_settings as _settings

    DEBUG = _settings.DEBUG
    BASE_DIR = _settings.BASE_DIR
    SECRET_KEY = _settings.SECRET_KEY
    ALLOWED_HOSTS = _settings.ALLOWED_HOSTS
    DATABASES = _settings.DATABASES
    STATIC_URL = _settings.STATIC_URL
    DJANGO_ADMIN_SITE_URL = _settings.DJANGO_ADMIN_SITE_URL
    EMAIL_SMTP_USERNAME = _settings.EMAIL_SMTP_USERNAME
    EMAIL_SMTP_PASSWORD = _settings.EMAIL_SMTP_PASSWORD
    EMAIL_SMTP_HOST = _settings.EMAIL_SMTP_HOST
    EMAIL_SMTP_PORT =  _settings.EMAIL_SMTP_PORT


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': ('%(asctime)s [%(process)d] [%(levelname)s] ' +
                       'pathname=%(pathname)s lineno=%(lineno)s ' +
                       'funcname=%(funcName)s %(message)s'),
            'datefmt': '%Y-%m-%d %H:%M:%S'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        }
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        }
    },
    'loggers': {
        'main_logger': {
            'handlers': ['console'],
            'level': 'INFO',
        }
    }
}
